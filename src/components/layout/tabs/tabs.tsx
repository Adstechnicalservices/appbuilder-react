
import React, { ReactElement } from 'react';
import { TabsProps } from './tabs-props';
import { Tab } from './tab/tab';
import { TabProps } from './tab/tab-props';

export class Tabs extends React.Component<TabsProps, {}> {
    render() {
            let key = 0;
            let cssClasses = "tabs";
            if (this.props.position == "bottom") {
                cssClasses += " bottom-tabs";
            } else {
                cssClasses += " top-tabs";
            }
            const tabWidth = (100/this.props.tabs.length);
            return <layout-tabs>
                <div className={cssClasses}>
                    {this.props.tabs.map(tab => {
                        return <Tab key={key++} {...tab} width={{ value: tabWidth, units: 'vw'}} />
                    })}
                </div>
            </layout-tabs>;
    }
}