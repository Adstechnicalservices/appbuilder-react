import React, { Ref, RefObject } from 'react';
import { TabProps } from './tab-props';
import { Icon } from '../../../presentational/icon/icon';
import { applyCommonProps } from '../../../../common/functions/apply-common-props';

export class Tab extends React.Component<TabProps, {}> {
    private ref: RefObject<HTMLDivElement> = React.createRef();
    animateTab() {
        const ref = this.ref.current;
        let classList:DOMTokenList;
        if (ref) {
          ref.classList.add('animate-tab');
          setTimeout(() => {
            ref.classList.remove('animate-tab')
          }, 140)
          
        }
    }

    render() {

        const el = <div className="tab" ref={this.ref} onClick={this.animateTab.bind(this)} style={{ backgroundColor: this.props.bgColor || '#f8f8f8', borderTop: '1px solid #e9e9f9'}}>
        
        <div className="tab-badge" style={{backgroundColor: this.props.badgeColor || 'red'}}>{this.props.badge && this.props.badge > 0 ? this.props.badge : null}</div>
       {this.props.icon ? <div className="tab-icon" style={{fill: this.props.iconColor || 'initial', stroke: this.props.iconBorderColor || 'initial'}}><Icon name={this.props.icon || ''} width={{ value:24, units:'px'}} height={{ value:24, units:'px'}} /> </div> : null } 
       <div className="tab-text"> {this.props.text} </div>
    </div>
       const tab = applyCommonProps(el, this.props);
        return <layout-tab>
                {tab}
             </layout-tab>
    }
}