import { ColorProps } from "../../../../common/interfaces/color-props";
import { SpacingProps } from "../../../../common/interfaces/spacing-props";
import { CommonProps } from "../../../../common/interfaces/common-props";

export interface TabProps extends CommonProps {
    badge?: number;
    badgeColor?: string;
    icon?: string;
    iconColor?: string;
    iconBorderColor?: string;
    text?: string;
    selected?: boolean;
    key?: number;
}