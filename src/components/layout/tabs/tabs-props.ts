import { ReactElement } from "react";
import { Tab } from "./tab/tab";
import { TabProps } from "./tab/tab-props";

export interface TabsProps {
    position: 'top' | 'bottom';
    children?: React.ReactElement<Tab>[];
    tabs: TabProps[];
}