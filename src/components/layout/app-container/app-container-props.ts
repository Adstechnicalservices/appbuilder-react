import { SvgSpriteProps } from "../../presentational/icon/svg-sprite/svg-sprite-props";
import { RouteProps, Route } from "react-router";
import { ComponentClass } from "react";

export interface AppContainerProps { 
    routes: RouteProps[];
    svgSprite?: SvgSpriteProps;
 }