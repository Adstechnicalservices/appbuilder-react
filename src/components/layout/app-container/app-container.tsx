import React from "react";
import { Alert } from "../../notifications/alert/alert";
import { Toast } from "../../notifications/toast/toast";
import { Confirm } from "../../notifications/confirm/confirm";
import { Modal } from "../../presentational/modal/modal";
import { ActionSheet } from "../../presentational/action-sheet/action-sheet";
import { AppContainerProps } from "./app-container-props";
import { SvgSprite } from "../../..";
import { Router, RouteProps, Route } from "react-router";
import createHistory from 'history/createBrowserHistory'

export class AppContainer extends React.Component<AppContainerProps, {}> {
    constructor(props: AppContainerProps) {
        super(props);
        console.info('Your app container is being instantiated.');
    }
    // Can be accessed by AppContainer.routerRef or AppContainer.alertRef etc.
    public static routerRef = React.createRef<Router>();
    public static history = createHistory();
    public static alertRef = React.createRef<Alert>();
    public static confirmRef = React.createRef<Confirm>();
    public static toastRef = React.createRef<Toast>();
    public static modalRef = React.createRef<Modal>();
    public static actionSheetRef = React.createRef<ActionSheet>();

    render() {
        const routes = this.props.routes;
            let routeKey = 0;
        return <app-container>
            <div className="app-container" style={{  minHeight: '100vh', height: '100%' }}>


                {/* Can use any SVG Sprite for icons */}
                {this.props.svgSprite ? <SvgSprite {...this.props.svgSprite} /> : null}

                {/* React Router routing */}
                <div title="routes">
                    {routes.map((route) => (
                        <Route key={routeKey++} {...route} />
                    ))}
                </div>

                {/* Top-level refs to control notifications and some presentational 
                elements through controllers */}

                <Alert ref={AppContainer.alertRef} />
                <Confirm ref={AppContainer.confirmRef} />
                <Toast ref={AppContainer.toastRef} />
                <Modal isVisible={false} ref={AppContainer.modalRef} />
                <ActionSheet ref={AppContainer.actionSheetRef} />

                {/* Render everything inside the <AppContainer></AppContainer> tag */}
                {this.props.children}
            </div>

        </app-container>
    }
}