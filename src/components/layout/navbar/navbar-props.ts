import { Photo } from "../../presentational/photo/photo";
import { ButtonProps } from "../../buttons/button/button-props";

export interface NavbarProps {
    leftIconToggle: 'hamburger-menu' | 'back-button' | 'none';
    logo: string | Photo;
    rightButtons?: ButtonProps[];
    onBackButtonClicked?: () => any;
}