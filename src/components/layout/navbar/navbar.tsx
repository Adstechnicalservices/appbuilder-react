import React, { MouseEvent, ReactElement } from "react";
import { AppContainer } from "../../layout/app-container/app-container";
import { NavbarProps } from "./navbar-props";
import { Icon } from "../../presentational/icon/icon";
import { BackButton } from "../../buttons/back-button/back-button";
import { Button } from "../../buttons/button/button";

export class Navbar extends React.Component<NavbarProps, {}> {
    constructor(props: NavbarProps) {
        super(props);
    }
    

    render() {

        return <layout-navbar>
            <div className="navbar">
            {/* If hamburger menu to show */}
                {this.props.leftIconToggle == 'hamburger-menu' ? <Icon name="star" /> : null }

                {/* If back button to show */}
                {this.props.leftIconToggle == 'back-button' && this.props.onBackButtonClicked ? <BackButton onBackButtonClicked={this.props.onBackButtonClicked.bind(this)} /> : null }
               
               {/* If has right menu of buttons */}
               {this.props.rightButtons ? <div className="right-menu">
                    {this.props.rightButtons.map(button => {
                        <Button {...button}  />
                    })}
                </div> : null }
                
            </div>
        </layout-navbar>
    }
}