import React from 'react';
import { TileProps } from './tile-props';
import { applyCommonProps } from '../../../common/functions/apply-common-props';
import { handleResponsiveSize } from '../../../common/functions/handle-responsive-size';
import { ResponsiveSizing } from '../../../common/interfaces/size-props';

export class Tile extends React.Component<TileProps, { width: string, height: string } > {
    private listener: ((event: UIEvent) => void) | undefined;
    constructor(props: TileProps) {
        super(props);
        this.state = {
            width: 'auto',
            height: 'auto'
        }
    }
    componentDidMount() {
        const responsiveSizing = (this.props.width as ResponsiveSizing);

        if (responsiveSizing) {
            this.setState({
                width: handleResponsiveSize(responsiveSizing)
            });

            this.listener = (event: UIEvent) => {
                this.setState({
                    width: handleResponsiveSize(responsiveSizing)
                })
            };
            window.addEventListener('resize', this.listener);
        }

    }

    componentWillUnmount() {
        if (this.listener)
            window.removeEventListener('resize', this.listener);
    }
    render() {
        const el = <div style={{width: this.state.width, height: this.state.height, backgroundColor: this.props.bgColor, color: this.props.textColor }}>
            {this.props.children}
        </div>;
        const tile = applyCommonProps(el, this.props);
        return <layout-tile>
            {tile}
        </layout-tile>;
    }
}