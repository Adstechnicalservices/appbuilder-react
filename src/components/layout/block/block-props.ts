import { HTMLAttributes } from "react";
import { CommonProps } from "../../../common/interfaces/common-props";

export interface BlockProps extends CommonProps {
    htmlAttributes?: HTMLAttributes<HTMLDivElement>;
    alignment?: 'top-left' | 'top-center' | 'top-right' | 'middle-left' | 'middle' | 'middle-right' | 'bottom-left' | 'bottom-center' | 'bottom-right';
    wrapContent?: boolean;
}