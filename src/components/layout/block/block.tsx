import React, { ReactElement } from 'react';
import { applyCommonProps } from '../../../common/functions/apply-common-props';
import { handleResponsiveSize } from '../../../common/functions/handle-responsive-size';
import { ResponsiveSizing } from '../../../common/interfaces/size-props';
import { BlockProps } from './block-props';

export class Block extends React.Component<BlockProps, { width: string, height: string } > {
    private listener: ((event: UIEvent) => void) | undefined;
    constructor(props: BlockProps) {
        super(props);
        this.state = {
            width: 'auto',
            height: 'auto'
        }
    }
    componentDidMount() {
        const responsiveSizing = (this.props.width as ResponsiveSizing);

        if (responsiveSizing) {
            this.setState({
                width: handleResponsiveSize(responsiveSizing)
            });

            this.listener = (event: UIEvent) => {
                this.setState({
                    width: handleResponsiveSize(responsiveSizing)
                })
            };
            window.addEventListener('resize', this.listener);
        }

    }

    componentWillUnmount() {
        if (this.listener)
            window.removeEventListener('resize', this.listener);
    }
    render() {
        const el = <div style={{ width: this.state.width }} {...this.props.htmlAttributes}>
            {this.props.children}
        </div>;
        const block: ReactElement<HTMLDivElement> = applyCommonProps(el, this.props);
        block.props.style.display = "flex";
        if(this.props.wrapContent) {
            block.props.style.flexWrap = "wrap";
        }
        if (this.props.alignment) {
            switch(this.props.alignment) {
                case 'bottom-center': {
                    block.props.style.alignItems = "flex-end";
                    block.props.style.justifyContent = "center";
                    break;
                }
                case "bottom-left": {
                    block.props.style.alignItems = "flex-end";
                    block.props.style.justifyContent = "flex-start";
                    break;                
                }
                case "bottom-right": {
                    block.props.style.alignItems = "flex-end";
                    block.props.style.justifyContent = "flex-end";
                    break;
                }
                case "middle": {
                    block.props.style.alignItems = "center";
                    block.props.style.justifyContent = "center";
                    break;
                }
                case "middle-left": {
                    block.props.style.alignItems = "center";
                    block.props.style.justifyContent = "flex-start";
                    break;
                }
                case "middle-right": {
                    block.props.style.alignItems = "center";
                    block.props.style.justifyContent = "flex-end";
                    break;
                }
                case "top-center": {
                    block.props.style.alignItems = "flex-start";
                    block.props.style.justifyContent = "center";
                    break;
                }
                case "top-left": {
                    block.props.style.alignItems = "flex-start";
                    block.props.style.justifyContent = "flex-start";
                    break;
                }
                case "top-right": {
                    block.props.style.alignItems = "flex-start";
                    block.props.style.justifyContent = "flex-end";
                    break;
                }
                default: {}
            }
        }
        
        return <layout-block>
            {block}
        </layout-block>;
    }
}