import React, { Ref } from "react";
import { AppContainer } from "../app-container/app-container";
import { PageProps } from "./page-props";
import { PageState } from "./page-state";
import { applyCommonProps } from "../../../common/functions/apply-common-props";
import { Navbar } from "../navbar/navbar";

export class Page extends React.Component<PageProps, PageState> {
    private pageEl = React.createRef<HTMLDivElement>();
    constructor(props: PageProps) {
        super(props);
        this.state = { 
            isNavigatingBack: undefined
        };

    }


    async handleBackButtonClicked() {
        console.log('Handle back button clicked')
         await this.animateTransition('back');
        AppContainer.history.goBack();
    }

    async animateTransition(direction: 'forward' | 'back') {
        return new Promise((resolve, reject) => {
            if (this.pageEl.current) {
                const classList = this.pageEl.current.classList;
                
                if (direction == 'back') {
                    classList.remove('slideInUp');
                    classList.add('slideOutDown',);              
                } 
                else {
                    classList.remove('slideOutDown');
                    classList.add('slideInUp');
                }
            } 
            // Give it time for animations to complete
            setTimeout(() => {
                resolve();
            }, 350);
        });

    }

    componentDidMount() {

    }

    render() {
        AppContainer.history.listen(async (location, action) => {
            if (action == 'POP') {
                console.log('POP.')

            }  
            if (action == 'REPLACE' || action == 'PUSH') {
                console.log('REPLACE OR PUSH')
                await this.animateTransition('forward');
            }
          }); 
            const el = <div ref={this.pageEl} 
            className="page animated slideInUp fast"
            style={{ minHeight: '100vh', height: '100%' }}
            >

               {/* Render all children elements  */}
                {this.props.children}
            </div>;

            const page = applyCommonProps(el, this.props);
                return <layout-page>
                    {
                        this.props.navbar 
                        ? <Navbar onBackButtonClicked={this.handleBackButtonClicked.bind(this)} {...this.props.navbar} /> 
                        : null 
                    
                    }
                    {
                        page
                    }
                </layout-page>
            
        
    }
}