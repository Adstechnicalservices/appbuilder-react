
export interface PageState { 
    isNavigatingBack: boolean | undefined;
}