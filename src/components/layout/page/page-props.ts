import { CommonProps } from "../../../common/interfaces/common-props";
import { NavbarProps } from "../navbar/navbar-props";


export interface PageProps extends CommonProps { 
    navbar?: NavbarProps;
 }