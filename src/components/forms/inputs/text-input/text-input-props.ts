import { ChangeEvent } from "react";
export interface TextInputProps { 
    onChange?: (event: ChangeEvent<HTMLInputElement>) => any;
    placeholder?: string;
    labelText?: string;
    value?: string;
}