import React, { MouseEvent } from "react";
import { TextInputProps } from "./text-input-props";

export class TextInput extends React.Component<TextInputProps, {}> {
    constructor(props: TextInputProps) {
        super(props);
        if (this.props.onChange) {
           this.props.onChange.bind(this);
        }
    }
    render() {
        return <text-input>
            <label>{this.props.labelText}</label>
                <input type="text" 
                value={this.props.value}
                placeholder={this.props.placeholder} 
                onChange={this.props.onChange} />
        </text-input>
    }
}