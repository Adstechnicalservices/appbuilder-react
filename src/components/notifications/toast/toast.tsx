import React, { MouseEvent, Ref, RefObject } from "react";
import { ToastParams } from "./toast-params";
import { ToastState } from "./toast-state";

export class Toast extends React.Component<{}, ToastState> {

    constructor(props: {}) {
        super(props);
        this.state = {
            isVisible: false,
            message: '',
            buttonText: undefined,
            duration: 2000,
            onDidDismiss: undefined,
            classList: "toast animated slideInUp faster"
        }
    }

    async present(instanceDetails: ToastParams) {
       console.log(instanceDetails);
        await this.setState({
            isVisible: true,
            message: instanceDetails.message,
            buttonText: instanceDetails.dismissButton ? instanceDetails.dismissButton.text : undefined,
            duration: instanceDetails.duration || this.state.duration,
            onDidDismiss: instanceDetails.onDismiss
        });
        await this.dismiss();
        await this.setState({
            isVisible: false,
            message: '',
            buttonText: undefined,
            duration: 2000,
            onDidDismiss: undefined,
            classList: "toast animated slideInUp faster"
        });
    }

    async wait(numMilliseconds: number) {
        return new Promise((resolve, reject) => {
            setTimeout(()=> resolve(), numMilliseconds);
        });
    }

    async dismiss() {
        return new Promise((resolve, reject) => {
            console.log(this.state.duration);
                setTimeout(async () => {
                    await this.setState({
                        classList: "toast animated slideOutDown faster"
                    });
                    if (this.state.onDidDismiss) {
                        this.state.onDidDismiss();
                    }
                    await this.wait(500);
                    resolve();                    
                }, this.state.duration || 2000);
            
        });
    }

    async hide(event: Event) {
        event.preventDefault();
        await this.dismiss();
    }

    render() {
        if (!this.state.isVisible) {
            return null;
        } else {
            return <notifications-toast>
                <div className={this.state.classList}>
                    {
                        this.state.buttonText
                            ?
                                (<a href=""
                                    className="toast-button"
                                    onClick={this.hide.bind(this)}
                                >{this.state.buttonText || 'OK'}</a>)
                            : 
                                null
                    }
                    <p>{this.state.message}</p>

                </div>

            </notifications-toast>
        }
    }
}