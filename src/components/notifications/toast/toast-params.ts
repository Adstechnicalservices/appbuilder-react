

export interface ToastParams {
    message: string;
    duration?: number;
    onDismiss?: () => any;
    dismissButton?: {
        text: string;
    }
}