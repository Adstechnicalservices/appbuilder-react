
export interface ToastState { 
    isVisible: boolean;
     message: string;
      buttonText: string | undefined;
       duration: number;
       onDidDismiss?: () => any;
       classList: string;
     }