import React, { MouseEvent } from "react";
import { ConfirmParams } from "./confirm-params";
import { ConfirmState } from "./confirm-state";

export class Confirm extends React.Component<{}, ConfirmState> {
    private confirmRef = React.createRef<HTMLDivElement>();
    private bgRef = React.createRef<HTMLDivElement>();
    constructor(props: {}) {
        super(props);
        this.state = {
            isVisible: false,
            message: '',
            confirmButtonText: 'OK',
            cancelButtonText: 'Cancel'
        }
    }

    public present(instanceDetails: ConfirmParams) {
       // const alertDiv = this.alertRef.current;
      //  const bgDiv = this.bgRef.current;
        // if (alertDiv && bgDiv) {
        //    // bgDiv.classList.add('alert-bg');

        //     //alertDiv.classList.remove('alert-hidden');
        //     //alertDiv.classList.add('alert-visible', 'animated', 'zoomIn', 'fastest');
        // }
        this.setState({
            isVisible: true,
            message: instanceDetails.message || '',
            confirmButtonText: instanceDetails.confirmButtonText || this.state.confirmButtonText,
            cancelButtonText: instanceDetails.cancelButtonText || this.state.cancelButtonText
            
        });
    }

    private hide(event: Event) {
        event.preventDefault();
        // const alertDiv = this.alertRef.current;
        // const bgDiv = this.bgRef.current;
        // if (alertDiv && bgDiv) {
        //     bgDiv.classList.remove('alert-bg');
        //     alertDiv.classList.add('alert-hidden');
        //     alertDiv.classList.remove('alert-visible', 'animated', 'zoomIn', 'fastest');

        // }
        this.setState({
            isVisible: false,
            message: ''
        });
        // if (this.props.onDismiss) {
        //     this.props.onDismiss();
        // }
    }

    render() {
        if (!this.state.isVisible) {
            return <div></div>
        } else {
            return <notifications-confirm>
                <div className="confirm-bg" ref={this.bgRef}></div>
                <div ref={this.confirmRef} className="confirm animated zoomIn fastest">
                    <h3>Confirm</h3>
                    <p>{this.state.message}</p>
                    <div style={{display: 'inline-block', width: '100%'}}>
                    <a href=""
                        className="confirm-button"
                        onClick={this.hide.bind(this)}
                    >{this.state.confirmButtonText || 'OK'}</a>
                                        <a href=""
                        className="cancel-button"
                        onClick={this.hide.bind(this)}

                    >{this.state.cancelButtonText || 'Cancel'}</a>
                    </div>
                </div>

            </notifications-confirm>
        }
    }
}