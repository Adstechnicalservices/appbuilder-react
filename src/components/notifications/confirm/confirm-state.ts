
export interface ConfirmState { isVisible: boolean, message: string, confirmButtonText: string, cancelButtonText: string }