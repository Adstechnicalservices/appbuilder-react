

export interface ConfirmParams {
    message: string;
    confirmButtonText?: string;
    cancelButtonText?: string;
    onDismiss?: () => any;
}