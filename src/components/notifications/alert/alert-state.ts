
export interface AlertState { isVisible: boolean, message: string, buttonText: string }