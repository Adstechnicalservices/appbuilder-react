import React, { MouseEvent } from "react";
import { AlertParams} from "./alert-params";
import { AlertState } from "./alert-state";

export class Alert extends React.Component<{}, AlertState> {
    private alertRef = React.createRef<HTMLDivElement>();
    private bgRef = React.createRef<HTMLDivElement>();
    constructor(props: {}) {
        super(props);
        this.state = {
            isVisible: false,
            message: '',
            buttonText: 'OK'
        }
    }

    public present(instanceDetails: AlertParams) {
       // const alertDiv = this.alertRef.current;
      //  const bgDiv = this.bgRef.current;
        // if (alertDiv && bgDiv) {
        //    // bgDiv.classList.add('alert-bg');

        //     //alertDiv.classList.remove('alert-hidden');
        //     //alertDiv.classList.add('alert-visible', 'animated', 'zoomIn', 'fastest');
        // }
        this.setState({
            isVisible: true,
            message: instanceDetails.message || '',
            buttonText: instanceDetails.buttonText || ''
        });
    }

    private hide(event: Event) {
        event.preventDefault();
        // const alertDiv = this.alertRef.current;
        // const bgDiv = this.bgRef.current;
        // if (alertDiv && bgDiv) {
        //     bgDiv.classList.remove('alert-bg');
        //     alertDiv.classList.add('alert-hidden');
        //     alertDiv.classList.remove('alert-visible', 'animated', 'zoomIn', 'fastest');

        // }
        this.setState({
            isVisible: false,
            message: ''
        });
        // if (this.props.onDismiss) {
        //     this.props.onDismiss();
        // }
    }

    render() {
        if (!this.state.isVisible) {
            return <div></div>
        } else {
            return <notifications-alert>
                <div className="alert-bg" ref={this.bgRef}></div>
                <div ref={this.alertRef} className="alert animated zoomIn fastest">
                    <h3>Alert</h3>
                    <p>{this.state.message}</p>
                    <a href=""
                        className="alert-button"
                        onClick={this.hide.bind(this)}
                    >{this.state.buttonText || 'OK'}</a>
                </div>

            </notifications-alert>
        }
    }
}