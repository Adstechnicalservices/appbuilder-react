

export interface AlertParams {
    message: string;
    buttonText?: string;
    onDismiss?: () => any;
}