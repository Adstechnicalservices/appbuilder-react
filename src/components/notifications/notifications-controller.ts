import { AlertParams } from "./alert/alert-params";
import { ToastParams } from "./toast/toast-params";
import { AppContainer } from "../layout/app-container/app-container";
import { ConfirmParams } from "./confirm/confirm-params";



export const NotificationsController = {
    createAlert: (params: AlertParams) => {
        const alertRef = AppContainer.alertRef.current;
       if (alertRef) {
           alertRef.present(params);
       }
       // let el = document.getElementById('app-alert-placeholder');
      // return render(<Alert message={props.message} buttonText={props.buttonText} />, el) as Alert;
    },
    createPrompt: () => {

    },
    createConfirm: (params: ConfirmParams) => {
        const confirmRef = AppContainer.confirmRef.current;
       if (confirmRef) {
           confirmRef.present(params);
       }
    },
    createToast: async (params: ToastParams) => {
        console.log('Creating a toast');
        const toastRef = AppContainer.toastRef.current;
        if (toastRef) {
            console.log('We have a toast ref');
            if (params.dismissButton) {
               return await toastRef.present({ onDismiss: params.onDismiss, message: params.message, dismissButton: { text: params.dismissButton.text }, duration: params.duration}) 
            } else {
                return await toastRef.present({ onDismiss: params.onDismiss, message: params.message, duration: params.duration})
            }
            
        }
    //         const el = document.createElement('div');
    //         document.body.appendChild(el);
        
    //    const toast = render(<Toast {...props} />, el) as Toast;
    //    toast.present();
    //    setTimeout(() => {
    //           unmountComponentAtNode(el);
    //           el.remove();
    //    }, (props.duration || 2000) + 1000);   
    }
}