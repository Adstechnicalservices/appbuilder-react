

declare namespace JSX {
    export interface IntrinsicElements {
        'app-container': any,
        'app-button': any,
        'text-input': any,
        'tel-input': any,
        'email-input': any,
        'number-input': any,
        'password-input': any,
        'notifications-alert': any,
        'notifications-confirm': any,
        'notifications-toast': any;
        'presentational-modal': any,
        'presentational-card': any,
        'presentational-photo': any,
        'presentational-popover': any,
        'presentational-action-sheet': any,
        'buttons-hyperlink': any,
        'buttons-back-button': any,
        'buttons-floating-action-button': any,
        'buttons-button': any,
        'layout-block': any,
        'layout-tile': any,
        'layout-page': any,
        'layout-tabs': any,
        'layout-tab': any,
        'layout-navbar': any
    }
}