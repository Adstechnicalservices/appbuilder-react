import { ReactElement } from "react";
import { Icon } from "../icon/icon";
import { CommonProps } from "../../../common/interfaces/common-props";

export interface PopoverProps extends CommonProps {
    triggeringElement: {
        text?: string;
        icon?: ReactElement<Icon>;
    }
}