import React from 'react';
import { PopoverProps } from './popover-props';
import { PopoverState } from './popover-state';
import { applyCommonProps } from '../../../common/functions/apply-common-props';

export class Popover extends React.Component<PopoverProps, PopoverState> {
    constructor(props:PopoverProps) {
        super(props);
        this.state = {
            isVisible: false
        }
    }

    toggle(event: Event) {
        console.log('Toggling');
        if (event)
        event.preventDefault();
        this.setState({
            isVisible: !this.state.isVisible
        });
    }

    render() {
        const el = <div className="content animated zoomIn fastest">
            {this.props.children}
        </div>;
        const popoverContent = applyCommonProps(el, this.props);

        return  <presentational-popover>
           <div className="container">
        <a href="#" onClick={this.toggle.bind(this)} >{this.props.triggeringElement.icon} {this.props.triggeringElement.text}</a>
        {this.state.isVisible ? <div onClick={this.toggle.bind(this)}><div className="bg"></div>
            {popoverContent}
        </div> : null }
        
        </div>; 
        </presentational-popover>
    }
}