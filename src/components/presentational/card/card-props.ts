import { HTMLAttributes } from "react";
import { CommonProps } from "../../../common/interfaces/common-props";

export interface CardProps extends CommonProps {
    htmlAttributes?: HTMLAttributes<HTMLDivElement>;
}