import React, { MouseEvent } from "react";
import { Button } from "../../buttons/button/button";
import { CardProps } from "./card-props";
import { applyCommonProps } from "../../../common/functions/apply-common-props";
import { ResponsiveSizing } from "../../../common/interfaces/size-props";
import { handleResponsiveSize } from "../../../common/functions/handle-responsive-size";


export class Card extends React.Component<CardProps, { width: string}> {
    private listener: ((event: UIEvent) => void) | undefined;
    constructor(props: CardProps) {
        super(props);
        this.state = {
            width: 'auto'
        }
    }

    componentDidMount() {
        const responsiveSizing = (this.props.width as ResponsiveSizing);

        if (responsiveSizing) {
            this.setState({
                width: handleResponsiveSize(responsiveSizing)
            });

            this.listener = (event: UIEvent) => {
                this.setState({
                    width: handleResponsiveSize(responsiveSizing)
                })
            };
            window.addEventListener('resize', this.listener);
        }
    }

    componentWillUnmount() {
        if (this.listener)
            window.removeEventListener('resize', this.listener);
    }

    render() {
        const el = <div {...this.props.htmlAttributes} style={{ }} className="card animated slideInUp fastest">
            {this.props.children}
        </div>
        const card = applyCommonProps(el, this.props);
            return <presentational-card>
                {card}
            </presentational-card>
        
    }
}