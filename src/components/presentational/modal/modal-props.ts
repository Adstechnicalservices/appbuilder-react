import { Button } from "../../components";
import { ReactElement } from "react";
import { CommonProps } from "../../../common/interfaces/common-props";

export interface ModalProps extends CommonProps {
    isVisible: boolean;
    title?: string;
    leftButton?: ReactElement<Button>;
    rightButton?: ReactElement<Button>;
}