import { Button } from "../../components";
import { ReactElement } from "react";

export interface ModalState {
    isVisible: boolean;
    leftButton?: ReactElement<Button>;
    rightButton?: ReactElement<Button>;
    title?: string;
}