import React, { MouseEvent, ReactElement } from "react";
import { ModalState } from "./modal-state";
import { ModalProps } from "./modal-props";
import { Button } from "../../buttons/button/button";
import { applyCommonProps } from "../../../common/functions/apply-common-props";


export class Modal extends React.Component<ModalProps, ModalState> {
    private modalRef = React.createRef<HTMLDivElement>();
    constructor(props: ModalProps) {
        super(props);
        this.state = {
            isVisible: this.props.isVisible,
            title: this.props.title || '',
            leftButton: this.props.leftButton,
            rightButton: this.props.rightButton || <Button 
                bgColor="transparent" 
                text="Close" 
                htmlAttributes={{ style:{border: 'none'}, onClick: this.close.bind(this) }} 
            />
        }
    }

    private close() {
        console.log('Closing');
        this.setState({
            isVisible: false
        });
    }

    // To use instance on AppContainer
    public present(instanceDetails: ModalProps) {
        this.setState({
            isVisible: true,
            leftButton: instanceDetails.leftButton,
            title: instanceDetails.title,
            rightButton: instanceDetails.rightButton || this.state.rightButton
        });
    }

    render() {
        if (!this.state.isVisible) {
            return null;
        } else {
            const el: ReactElement<HTMLDivElement> = <div style={{}} ref={this.modalRef} className="modal animated slideInUp fastest">
            <div className="modal-bar">
                <span className="left">
                    {this.state.leftButton}
                </span>
                <span className="center">
                    {this.state.title}
                </span>
                <span className="right">
                    {this.state.rightButton}
                </span>
            </div>
                {this.props.children}
            </div>;

            const modal = applyCommonProps(el, this.props);

            return <presentational-modal>
                {modal}
            </presentational-modal>
        }
    }
}