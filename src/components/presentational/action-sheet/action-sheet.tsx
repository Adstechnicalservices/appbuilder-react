import React from 'react';
import { ActionSheetProps } from './action-sheet-props';
import { ActionSheetState } from './action-sheet-state';

export class ActionSheet extends React.Component<ActionSheetProps, ActionSheetState> {
    constructor(props: ActionSheetProps) {
        super(props);
        this.state = {
            isVisible: this.props.isVisible || false
        }
    }

    present(props: ActionSheetProps) {
        console.log(props.children)
        this.setState({
            isVisible: props.isVisible || true,
            instanceElements: props.children
        });
    }

    toggle(event: Event) {
        if (event) {
            event.preventDefault();
        }
        this.setState({
            isVisible : !this.state.isVisible
        });
    }

    render() {
        if (this.state.isVisible) {
            return <presentational-action-sheet>
                <div className="container" onClick={this.toggle.bind(this)}>
                    <div className="bg"></div>
                    <div className="action-sheet animated slideInUp fastest">
                        {this.props.children || this.state.instanceElements}
                    </div>
                </div>
            </presentational-action-sheet>
        } else {
            return null;
        }
    }
}