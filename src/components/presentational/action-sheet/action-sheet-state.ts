import { ReactElement } from "react";

export interface ActionSheetState {
    isVisible: boolean;
    instanceElements?: ReactElement<any>;
}