import { ReactElement } from "react";
import { CommonProps } from "../../../common/interfaces/common-props";

export interface ActionSheetProps extends CommonProps {
    isVisible?: boolean;
    children?: ReactElement<any>;
}