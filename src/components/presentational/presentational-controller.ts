import { AppContainer } from "../layout/app-container/app-container";
import { ModalProps } from "./modal/modal-props";
import { ActionSheetProps } from "./action-sheet/action-sheet-props";
import { PopoverProps } from "./popover/popover-props";




export const PresentationalController = {
    createActionSheet: (props: ActionSheetProps) => {
        const actionSheetRef = AppContainer.actionSheetRef.current;
       if (actionSheetRef) {
           actionSheetRef.present(props);
       }
    },
    createModal: (props: ModalProps) => {
        const modalRef = AppContainer.modalRef.current;
       if (modalRef) {
           modalRef.present(props);
       }
    }
}