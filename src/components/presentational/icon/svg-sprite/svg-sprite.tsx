import React, { ReactElement } from 'react';
import { SvgSpriteProps } from './svg-sprite-props';

export class SvgSprite extends React.Component<SvgSpriteProps, {}> {
    render() {
        return this.props.sprite;
    }
}