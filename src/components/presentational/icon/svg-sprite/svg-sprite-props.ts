import { ReactElement } from "react";

export interface SvgSpriteProps { 
    sprite: ReactElement<SVGElement> 
}