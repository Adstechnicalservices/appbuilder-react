import { CssUnits } from "../../../common/types/CssUnits";


export interface IconProps {
    name: string;
    width?: {
        value: number,
        units: CssUnits
    };
    height?: {
        value: number,
        units: CssUnits       
    };
    fill?: string;
    stroke?: string;
}