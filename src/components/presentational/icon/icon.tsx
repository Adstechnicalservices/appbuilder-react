import React, { ReactElement } from 'react';
import { IconProps } from './icon-props';

export class Icon extends React.Component<IconProps, {}> {
    render() {
        const el: ReactElement<SVGUseElement> = <use xlinkHref={"#" + this.props.name} style={{}} />;
        if (this.props.stroke) {
            el.props.style.stroke = this.props.stroke;
        }

        if (this.props.fill) {
            el.props.style.fill = this.props.fill;
        }
        if (this.props.width) {
            el.props.style.width = this.props.width.value + this.props.width.units + "!important";
        }
        if (this.props.height) {
            el.props.style.height = this.props.height.value + this.props.height.units;
        }

        return <svg 
            className="icon" 
            width={ this.props.width ? this.props.width.value + this.props.width.units : "32px" }
            height={ this.props.height ? this.props.height.value + this.props.height.units : "32px" }
        >
            {el}
        </svg>
    }
}