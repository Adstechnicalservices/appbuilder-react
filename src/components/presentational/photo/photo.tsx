import React, { MouseEvent, ReactElement } from "react";
import { AppContainer } from "../../layout/app-container/app-container";
import { PhotoProps } from "./photo-props";
import { applyCommonProps } from "../../../common/functions/apply-common-props";
import { ResponsiveSizing } from "../../../common/interfaces/size-props";
import { handleResponsiveSize } from "../../../common/functions/handle-responsive-size";

export class Photo extends React.Component<PhotoProps, {}> {
    private listener: ((event: UIEvent) => void) | undefined;
    constructor(props: PhotoProps) {
        super(props);
    }

    componentDidMount() {
        const responsiveSizing = (this.props.width as ResponsiveSizing);

        if (responsiveSizing) {
            this.setState({
                width: handleResponsiveSize(responsiveSizing)
            });

            this.listener = (event: UIEvent) => {
                this.setState({
                    width: handleResponsiveSize(responsiveSizing)
                })
            };
            window.addEventListener('resize', this.listener);
        }
    }

    componentWillUnmount() {
        if (this.listener)
            window.removeEventListener('resize', this.listener);
    }

    render() {
        
        const el: ReactElement<HTMLImageElement> = <img 
        src={this.props.src}
        style={{
            maxWidth: this.props.maxWidth ? this.props.maxWidth.value + this.props.maxWidth.units : '100%',
            maxHeight: this.props.maxHeight? this.props.maxHeight.value + this.props.maxHeight.units : '100vh'
        }}
        alt={this.props.alt}
        title={this.props.title || ""}
     />;
     if (this.props.rounded) {
         el.props.style.borderRadius = "50%";
     }

     const photo = applyCommonProps(el, this.props);

        return <presentational-photo>
            {photo}
        </presentational-photo>
    }
}