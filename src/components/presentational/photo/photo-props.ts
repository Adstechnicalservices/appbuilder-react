import { CssUnits } from "../../../common/types/CssUnits";
import { CommonProps } from "../../../common/interfaces/common-props";


export interface PhotoProps extends CommonProps {
    src: string;
    alt: string;

    maxWidth?: {
        value: number,
        units: CssUnits
    };
    maxHeight?: {
        value: number,
        units: CssUnits       
    };
    rounded?: boolean;
    
    title?: string;
}