import { ButtonHTMLAttributes } from "react";
import { CommonProps } from "../../../common/interfaces/common-props";

export interface ButtonProps extends CommonProps {
    text: string;
    htmlAttributes?: ButtonHTMLAttributes<HTMLButtonElement>;
}