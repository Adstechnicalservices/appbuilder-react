import React, { ReactElement } from 'react';
import { ButtonProps } from './button-props';
import { applyCommonProps } from '../../../common/functions/apply-common-props';
import { ResponsiveSizing } from '../../../common/interfaces/size-props';
import { handleResponsiveSize } from '../../../common/functions/handle-responsive-size';
export class Button extends React.Component<ButtonProps, { width: string }> {
    private listener: ((event: UIEvent) => void) | undefined;
    constructor(props: ButtonProps) {
        super(props);
        this.state = {
            width: 'auto'
        }
    }
    componentDidMount() {
         const responsiveSizing = (this.props.width as ResponsiveSizing);

        if (responsiveSizing) {
            this.setState({
                width: handleResponsiveSize(responsiveSizing)
            });

            this.listener = (event: UIEvent) => {
                this.setState({
                    width: handleResponsiveSize(responsiveSizing)
                })
            };
            window.addEventListener('resize', this.listener);
        }

    }

    componentWillUnmount() {
        if (this.listener)
            window.removeEventListener('resize', this.listener);
    }

    render() {
        const el = <button style={{}} {...this.props.htmlAttributes }>
            {this.props.text}
        </button> as ReactElement<HTMLButtonElement>;

        const button =  applyCommonProps(el, this.props);
            
        return <app-button>
            {button}
        </app-button>
    }
}