import React from "react";
import { Link } from "react-router-dom";

export class Hyperlink extends React.Component<{ linkText: string, location: string}, {}> {
    constructor(props: { linkText: string, location: string }) {
        super(props);
    }
    private handleClick(event: React.FormEvent<HTMLAnchorElement>) {
        console.log('Clicked')
        console.log('Going forward');
    }
    render() {
        return <buttons-hyperlink> 
            <Link  to={this.props.location } onClick={ this.handleClick.bind(this)}>
            {this.props.linkText}
            </Link>
            </buttons-hyperlink>
    }
}