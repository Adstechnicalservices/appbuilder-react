import React, { MouseEvent } from "react";
import { AppContainer } from "../../layout/app-container/app-container";

export class BackButton extends React.Component<{ onBackButtonClicked: () => any }, {}> {
    constructor(props: { onBackButtonClicked: () => any }) {
        super(props);
    }
    private onClick(event: React.FormEvent<HTMLAnchorElement>) {
        event.preventDefault();
        console.log('Going back');
        this.props.onBackButtonClicked();
        //AppContainer.history.goBack();
    }
    render() {

        return <buttons-back-button>
            <div>
                <a href="" onClick={this.onClick.bind(this)}>Back</a>
            </div>
        </buttons-back-button>
    }
}