import { CommonProps } from "../interfaces/common-props";
import { ReactElement } from "react";
import { handleResponsiveSize } from "./handle-responsive-size";
import { BasicSizing, ResponsiveSizing } from "../interfaces/size-props";
import { handleNamedSize } from "./handle-named-size";



export const applyCommonProps = <T extends HTMLElement, U extends CommonProps>(el: ReactElement<T>, props: U): ReactElement<T> => {
    if (el.props && el.props.style) {
        //ColorProps
        if (props.bgColor) {
            el.props.style.backgroundColor = props.bgColor;
        }
        if (props.textColor) {
            el.props.style.color = props.textColor;
        }
        if (props.fill) {
            el.props.style.fill = props.fill;
        }
        if (props.stroke) {
            el.props.style.stroke = props.stroke;
        }
        //SizeProps
        if (props.width) {
            // Named size
            if (typeof props.width === 'string') {
                el.props.style.width = handleNamedSize(props.width);
            }
            // Basic size
            else if ((props.width as BasicSizing).value != undefined) {
                const basicSize = (props.width as BasicSizing);
                el.props.style.width = basicSize.value + basicSize.units;
            } 
            // Responsive size
            else {
                const responsiveSize = (props.width as ResponsiveSizing);
                el.props.style.width = handleResponsiveSize(responsiveSize);
            }
        }
        //SpacingProps
            el.props.style.marginTop = props.marginTop? props.marginTop.value + props.marginTop.units : '0';
            el.props.style.marginRight = props.marginRight? props.marginRight.value + props.marginRight.units : '0';
            el.props.style.marginLeft = props.marginLeft? props.marginLeft.value + props.marginLeft.units : '0';
            el.props.style.marginBottom = props.marginBottom? props.marginBottom.value + props.marginBottom.units : '0';
        
            el.props.style.paddingTop = props.paddingTop? props.paddingTop.value + props.paddingTop.units : '0';
            el.props.style.paddingRight = props.paddingRight? props.paddingRight.value + props.paddingRight.units : '0';
            el.props.style.paddingLeft = props.paddingLeft? props.paddingLeft.value + props.paddingLeft.units : '0';
            el.props.style.paddingBottom = props.paddingBottom? props.paddingBottom.value + props.paddingBottom.units : '0';          
        
    }
    return el;
}