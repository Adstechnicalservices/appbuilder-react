import { NamedSizing } from "../interfaces/size-props";

export const handleNamedSize = (namedSize: NamedSizing): string => {
    let size = '';
    if (namedSize == 'one-quarter') {
        size = '25vw';
    }
    if (namedSize == 'half') {
        size = '50vw';
    }
    if (namedSize == 'three-quarters') {
        size = '75vw';
    }
    if (namedSize == 'full') {
        size = '100vw';
    }
    return size;
}