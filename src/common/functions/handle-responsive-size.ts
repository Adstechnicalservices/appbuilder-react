import { WIDTH_BREAKPOINTS } from "../constants/width-breakpoints";
import { ResponsiveSizing } from "../interfaces/size-props";


export const handleResponsiveSize = (responsiveSizing: ResponsiveSizing): string => {
        if (window.innerWidth <= WIDTH_BREAKPOINTS.mobile.max) {
            console.log('mobile');
            // this.setState({
            //     responsiveWidth: responsiveSizing.mobile ? responsiveSizing.mobile.width : 100
            // });
            if (responsiveSizing.mobile) {
                if (typeof responsiveSizing.mobile === 'number') {
                    return responsiveSizing.mobile + '%';
                } else {
                    if (responsiveSizing.mobile == 'full') {
                        return 100 + '%';
                    }
                    if (responsiveSizing.mobile == 'half') {
                        return 50 + '%';
                    }
                    if (responsiveSizing.mobile == 'one-quarter') {
                        return 25 + '%';
                    }

                    if (responsiveSizing.mobile == 'three-quarters') {
                        return 75 + '%';
                    }
                }
            } 
        }
        if (window.innerWidth >= WIDTH_BREAKPOINTS.phablet.min && window.innerWidth <= WIDTH_BREAKPOINTS.phablet.max) {
            console.log('phablet');
            return responsiveSizing.phablet ? responsiveSizing.phablet + '%' : 100 + '%';
        }
        if (( window.innerWidth >= WIDTH_BREAKPOINTS.tablet.min && window.innerWidth <= WIDTH_BREAKPOINTS.tablet.max)) {
            console.log('tablet')
            return responsiveSizing.tablet ? responsiveSizing.tablet + '%' : 100 + '%';
        }
        return 100 + '%';
}