
export type CssUnits = 'px' | '%' | 'vw' | 'vh' | 'pt' | 'em' | 'rem';