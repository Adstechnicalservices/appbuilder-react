import { CssUnits } from "./CssUnits";

export type CssMeasurement = {
    value: number;
    units: CssUnits
}