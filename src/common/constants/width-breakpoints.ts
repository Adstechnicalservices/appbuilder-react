
/**
 * All expressed in px
 */
export const WIDTH_BREAKPOINTS = {
    mobile: {
        min: 0,
        max: 479
    },
    phablet: {
        min: 480,
        max: 767
    },
    tablet: {
        min: 768,
        max: 1023
    }
}