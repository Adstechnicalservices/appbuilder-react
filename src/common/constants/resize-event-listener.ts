import { TypedEvent } from "../interfaces/typed-event-emitter";

const resizeEvent = new TypedEvent<{ width: string, height: string}>();
export const resizeEventListener = () => {
    
    window.addEventListener('resize', (event: UIEvent) => {

    });
    return resizeEvent;
}