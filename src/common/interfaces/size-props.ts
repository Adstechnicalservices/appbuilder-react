import { CssUnits } from "../types/CssUnits";
import { CssMeasurement } from "../types/CssMeasurement";

export interface SizeProps {
   width?: Size;
   height?: Size;
}

export type NamedSizing = 'one-quarter' | 'half' | 'three-quarters' | 'full';
export type BasicSizing = CssMeasurement;

export interface ResponsiveSizing {
    mobile?: number | NamedSizing;
    phablet?: number | NamedSizing;
    tablet?: number | NamedSizing;
    desktopSm?: number | NamedSizing;
    desktopMed?: number | NamedSizing;
    desktopLg?: number | NamedSizing;
}


export interface Width {
    value: number;
    units: CssUnits;
}

export interface Height {
    value: number;
    units: CssUnits;
}

export type Size = NamedSizing | BasicSizing | ResponsiveSizing;