import { CssMeasurement } from "../types/CssMeasurement";

export interface SpacingProps {
    marginLeft?: CssMeasurement,
    marginRight?: CssMeasurement,
    marginTop?: CssMeasurement,
    marginBottom?: CssMeasurement
    paddingLeft?: CssMeasurement,
    paddingRight?: CssMeasurement,
    paddingTop?: CssMeasurement,
    paddingBottom?: CssMeasurement
}