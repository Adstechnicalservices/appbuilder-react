import { ColorProps } from "./color-props";
import { SizeProps } from "./size-props";
import { SpacingProps } from "./spacing-props";


export interface CommonProps extends ColorProps, SizeProps, SpacingProps {}