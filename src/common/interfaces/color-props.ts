
export interface ColorProps {
    bgColor?: string;
    textColor?: string;
    fill?: string;
    stroke?: string;
}