// Import React
import React from  'react';

// Import ReactDOM
import ReactDOM from  'react-dom';

import { BrowserRouter } from 'react-router-dom';
import { Hyperlink, Button, TextInput, Tile, Card, Photo, Icon, ActionSheet, Popover, Page, Block, AppContainer, SvgSprite } from '../src';
import { Tabs } from '../src/components/layout/tabs/tabs';
import { Tab } from '../src/components/layout/tabs/tab/tab';
import { Navbar } from '../src/components/layout/navbar/navbar';

export class HomePage extends React.Component<{}, {}>{
    constructor(props: {}) {
        super(props);
    }
    render() {
        return <Page>
        <Block width="full" height={ { units: 'px', value: 199}} bgColor="red" />
                    <Tabs position="bottom" tabs={[{ selected: true, badge: 4, icon: 'star', iconColor: 'blue', iconBorderColor: 'blue',  text: 'Tab 1', textColor: 'blue' }, { key: 2, text: 'Tab 2'}]} />
            <div style={{ backgroundColor: 'green' }} title="home">
                <p>Home page</p>
                <Hyperlink location="/about" linkText="About" />
            </div>

        </Page>
    }
}

export class AboutPage extends React.Component<{}, { firstName: string, showActionSheet: boolean }> {
    constructor(props: {}) {
        super(props);
        this.state = {
            firstName: 'Joe',
            showActionSheet: false
        }
    }

    printState() {
        console.log(this.state);

    }

    showActionSheet() {
        this.setState({
            showActionSheet: true
        })
        // PresentationalController.createActionSheet({
        //     isVisible: true,
        //        children: <div><Button text="test" handleClick={this.printState.bind(this)} bgColor="orange" color="purple"/><Button text="test" handleClick={this.printState.bind(this)} color="blue" bgColor="white" /></div>
        //    })
    }

     showModal() {
    //     PresentationalController.createModal({ title: 'Testing', leftButton: <Button text='close' handleClick = {this.printState.bind(this)} /> });
     }

    async showConfirm() {
       // NotificationsController.createConfirm({ message: 'Are you sure?'});
    }

    async showAlert() {
      //  NotificationsController.createAlert({ message: 'This is new'});
    }

    async showToasts() {
        const messages = ['This is one', 'This is two', 'This is three', 'This is four'];
        for (let m of messages) {
          //  await NotificationsController.createToast({ message: m, duration: 1000, dismissButton: { text: 'Got it!'} });
        }         
    }

    render() {
        return  <Page bgColor='none' textColor='darkblue' navbar={{ leftIconToggle: 'back-button', logo: 'test'}}>  
        {/* Page content */}
        ...<h2>About Page</h2>
        <Block htmlAttributes={{style: {height: '100px', border: '1px solid red'}}} alignment="middle">
            This is some text content
        </Block>
        <Block wrapContent={true}>
            <Button width="full" textColor='purple' bgColor='blue' text="Show Alert" htmlAttributes= {{ onClick: this.showAlert.bind(this), title: 'Orange button'}} />
            <Button   text="Show Toasts" htmlAttributes={{ onClick: this.showToasts.bind(this)}} />
            <Button width={{mobile: "full", phablet:"three-quarters", tablet:"half" }}  text="Show Action Sheet" htmlAttributes={{ onClick: this.showActionSheet.bind(this)}} />
            <Button text="Show Confirm" htmlAttributes={{ onClick: this.showConfirm.bind(this)}} />
            <Button text="Show Modal" htmlAttributes={{ onClick: this.showModal.bind(this)}} />
        </Block>
        <Hyperlink location="/" linkText="Home" />
        < br />
        <TextInput placeholder="This is your first name" onChange={evt => this.setState({ firstName: evt.target.value })} />
        <TextInput value={this.state.firstName} labelText="First Name" onChange={evt => this.setState({ firstName: evt.target.value })} />
        <br />
        <Tile textColor="white" bgColor='darkblue' width="full" paddingBottom= {{value: 100, units: 'px'}} paddingTop={{value: 100, units: 'px'}}>
            {/* <TextInput value="30" onChange={evt => this.setState({ firstName: evt.target.value })} /> */}
            <h2 className="center">30</h2>
            <p className="center">Followers</p>
        </Tile>
        <Tile bgColor="purple" textColor="white"></Tile>
        <Tile>
            <TextInput value="60" onChange={evt => this.setState({ firstName: evt.target.value })} />
        </Tile>
        <Card >
            <h2>Test Card</h2>
            <p>This is a test card</p>
            <Photo
            alt="McLaren Senna orange color"
            width = { {value: 22, units: 'vw'}}
            rounded={true}
             src="https://media.caranddriver.com/images/17q4/692996/2019-mclaren-senna-hypercar-official-photos-and-info-news-car-and-driver-photo-698055-s-original.jpg" />
        </Card>
        {/* <Tabs>
            <Tab />
            <Tab />
        </Tabs> */}
        <Icon name="star"
            width={{ value: 64, units: 'px'}}
            height={{ value: 64, units: 'px'}}
            fill="green"
            stroke="yellow"
        />

        {this.state.showActionSheet ? 
        <ActionSheet isVisible={true}>
            <Button text="View State" htmlAttributes={{ onClick: this.printState.bind(this)}} />
        </ActionSheet> : null
    }
        
            
            <Popover triggeringElement={{ icon:<Icon name="star" />
            }}> <Button text="View State" htmlAttributes={{ onClick: this.printState.bind(this)}} /> </Popover>

      </Page>
    }
}


export class App extends React.Component<{}, { isReady: boolean }> {

    constructor(props: {}) {
        super(props);
        this.state = { isReady: false };
    }

    componentDidMount() {

    }
    render() {
        return  <AppContainer routes={[
               { path: "/", component: HomePage, exact: true},
               { path:"/about", component: AboutPage}
            ]} 
                svgSprite={{sprite: <SvgSprite sprite={<svg 
                    xmlns="http://www.w3.org/2000/svg"  
                    style={{ display: 'none' }}><symbol id="pin" viewBox="0 0 54.757 54.757"><title>pin</title><path d="M40.94,5.617C37.318,1.995,32.502,0,27.38,0c-5.123,0-9.938,1.995-13.56,5.617c-6.703,6.702-7.536,19.312-1.804,26.952 L27.38,54.757L42.721,32.6C48.476,24.929,47.643,12.319,40.94,5.617z M27.557,26c-3.859,0-7-3.141-7-7s3.141-7,7-7s7,3.141,7,7 S31.416,26,27.557,26z"/></symbol><symbol id="search" viewBox="0 0 56.966 56.966"><title>search</title><path d="M55.146,51.887L41.588,37.786c3.486-4.144,5.396-9.358,5.396-14.786c0-12.682-10.318-23-23-23s-23,10.318-23,23 s10.318,23,23,23c4.761,0,9.298-1.436,13.177-4.162l13.661,14.208c0.571,0.593,1.339,0.92,2.162,0.92 c0.779,0,1.518-0.297,2.079-0.837C56.255,54.982,56.293,53.08,55.146,51.887z M23.984,6c9.374,0,17,7.626,17,17s-7.626,17-17,17 s-17-7.626-17-17S14.61,6,23.984,6z"/></symbol><symbol id="star" viewBox="0 0 49.94 49.94"><title>star</title><path d="M48.856,22.73c0.983-0.958,1.33-2.364,0.906-3.671c-0.425-1.307-1.532-2.24-2.892-2.438l-12.092-1.757 c-0.515-0.075-0.96-0.398-1.19-0.865L28.182,3.043c-0.607-1.231-1.839-1.996-3.212-1.996c-1.372,0-2.604,0.765-3.211,1.996 L16.352,14c-0.23,0.467-0.676,0.79-1.191,0.865L3.069,16.622c-1.359,0.197-2.467,1.131-2.892,2.438 c-0.424,1.307-0.077,2.713,0.906,3.671l8.749,8.528c0.373,0.364,0.544,0.888,0.456,1.4L8.224,44.701 c-0.183,1.06,0.095,2.091,0.781,2.904c1.066,1.267,2.927,1.653,4.415,0.871l10.814-5.686c0.452-0.237,1.021-0.235,1.472,0 l10.815,5.686c0.526,0.277,1.087,0.417,1.666,0.417c1.057,0,2.059-0.47,2.748-1.288c0.687-0.813,0.964-1.846,0.781-2.904 l-2.065-12.042c-0.088-0.513,0.083-1.036,0.456-1.4L48.856,22.73z"/></symbol><symbol id="user" viewBox="0 0 60 60"><title>user</title><path d="M48.014,42.889l-9.553-4.776C37.56,37.662,37,36.756,37,35.748v-3.381c0.229-0.28,0.47-0.599,0.719-0.951 c1.239-1.75,2.232-3.698,2.954-5.799C42.084,24.97,43,23.575,43,22v-4c0-0.963-0.36-1.896-1-2.625v-5.319 c0.056-0.55,0.276-3.824-2.092-6.525C37.854,1.188,34.521,0,30,0s-7.854,1.188-9.908,3.53C17.724,6.231,17.944,9.506,18,10.056 v5.319c-0.64,0.729-1,1.662-1,2.625v4c0,1.217,0.553,2.352,1.497,3.109c0.916,3.627,2.833,6.36,3.503,7.237v3.309 c0,0.968-0.528,1.856-1.377,2.32l-8.921,4.866C8.801,44.424,7,47.458,7,50.762V54c0,4.746,15.045,6,23,6s23-1.254,23-6v-3.043 C53,47.519,51.089,44.427,48.014,42.889z"/></symbol><symbol id="photograph" viewBox="0 0 60 60"><title>photograph</title><path d="M30,20.5c-6.617,0-12,5.383-12,12s5.383,12,12,12s12-5.383,12-12S36.617,20.5,30,20.5z"/><path d="M55.201,15.5h-8.524l-4-10H17.323l-4,10H12v-5H6v5H4.799C2.152,15.5,0,17.652,0,20.299v29.368 C0,52.332,2.168,54.5,4.833,54.5h50.334c2.665,0,4.833-2.168,4.833-4.833V20.299C60,17.652,57.848,15.5,55.201,15.5z M10,15.5H8v-3 h2V15.5z M30,50.5c-9.925,0-18-8.075-18-18s8.075-18,18-18s18,8.075,18,18S39.925,50.5,30,50.5z M52,27.5c-2.206,0-4-1.794-4-4 s1.794-4,4-4s4,1.794,4,4S54.206,27.5,52,27.5z"/></symbol><symbol id="gear" viewBox="0 0 54 54"><title>gear</title><path d="M53.188,23.518l-3.128-0.602c-1.842-0.354-3.351-1.607-4.035-3.354c-0.686-1.745-0.433-3.69,0.677-5.203l1.964-2.679 c0.292-0.397,0.249-0.949-0.1-1.298l-4.242-4.242c-0.339-0.339-0.871-0.39-1.268-0.121l-2.638,1.786 c-0.91,0.616-1.958,0.942-3.033,0.942c-2.713,0-4.98-1.941-5.393-4.617l-0.505-3.283C31.414,0.36,30.994,0,30.5,0h-6 c-0.479,0-0.892,0.341-0.982,0.812l-0.777,4.041c-0.492,2.559-2.746,4.416-5.357,4.416c-1.075,0-2.125-0.325-3.033-0.941 L10.944,6.02c-0.397-0.268-0.929-0.218-1.268,0.121l-4.243,4.242c-0.349,0.349-0.391,0.9-0.1,1.299l1.964,2.679 c1.109,1.512,1.362,3.457,0.677,5.203c-0.686,1.745-2.194,2.999-4.036,3.353l-3.128,0.602C0.34,23.608,0,24.021,0,24.5v6 c0,0.493,0.36,0.913,0.848,0.988l3.283,0.505c1.853,0.285,3.408,1.481,4.157,3.2c0.75,1.72,0.57,3.673-0.482,5.226L6.02,43.057 c-0.269,0.396-0.218,0.929,0.121,1.268l4.242,4.242c0.349,0.348,0.899,0.393,1.298,0.1l2.679-1.964 c0.944-0.692,2.05-1.059,3.197-1.059c2.613,0,4.867,1.857,5.359,4.416l0.602,3.129C23.608,53.659,24.021,54,24.5,54h6 c0.494,0,0.913-0.36,0.988-0.848l0.355-2.309c0.466-3.032,3.067-4.618,5.396-4.618c1.146,0,2.25,0.366,3.196,1.06l1.884,1.381 c0.399,0.293,0.95,0.248,1.298-0.1l4.242-4.242c0.339-0.339,0.39-0.871,0.121-1.268l-1.786-2.638 c-1.052-1.553-1.232-3.506-0.482-5.226c0.75-1.719,2.304-2.915,4.157-3.2l3.283-0.505C53.64,31.413,54,30.993,54,30.5v-6 C54,24.021,53.66,23.608,53.188,23.518z M36,27.5c0,4.687-3.813,8.5-8.5,8.5S19,32.187,19,27.5s3.813-8.5,8.5-8.5 S36,22.813,36,27.5z"/><path d="M27.5,22c-3.033,0-5.5,2.468-5.5,5.5s2.467,5.5,5.5,5.5s5.5-2.468,5.5-5.5S30.533,22,27.5,22z"/></symbol></svg> } />}}
            >
                
            </AppContainer>
    }
}




// Mount React App

ReactDOM.render(
   <BrowserRouter>
    <App />
   </BrowserRouter>,
  document.getElementById('app')
)